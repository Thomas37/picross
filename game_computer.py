from processes import *
from utils import *


def process_column(game, column_number, column_numbers):
    column = get_column(game, column_number)
    column = process_fill_commons(column, column_numbers)
    column = process_fill_ends(column, column_numbers)
    column = process_fill_ends_reverse(column, column_numbers)
    column = process_fill_full_lines(column, column_numbers)
    column = process_fill_completed_lines(column, column_numbers)
    set_column(game, column_number, column)


def process_row(game, row_number, row_numbers):
    row = get_row(game, row_number)
    row = process_fill_commons(row, row_numbers)
    row = process_fill_ends(row, row_numbers)
    row = process_fill_full_lines(row, row_numbers)
    row = process_fill_completed_lines(row, row_numbers)
    set_row(game, row_number, row)


def compute_game(length, cols, rows):
    current = [[None] * length] * length
    prev = None
    while prev != current:
        prev = current.copy()
        for i in range(len(cols)):
            process_column(current, i, cols[i])
        for i in range(len(rows)):
            process_row(current, i, rows[i])
    return current
