from game_computer import compute_game, fill_numbers, print_game

if __name__ == '__main__':

    length = int(input("Length : "))

    print("Input of all " + str(length) + " columns numbers")
    cols = []
    fill_numbers(cols, length)

    print("Input of all " + str(length) + " rows numbers")
    rows = []
    fill_numbers(rows, length)

    print_game(compute_game(length, cols, rows))
