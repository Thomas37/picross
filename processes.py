from utils import is_valid_line


def process_fill_ends_reverse(line, numbers):
    work_line = line.copy()
    work_numbers = numbers.copy()
    work_line.reverse()
    work_numbers.reverse()
    res = process_fill_ends(work_line, work_numbers)
    res.reverse()
    return res


def process_fill_ends(line, numbers):
    if None not in line:
        return line
    if len(numbers) == 0:
        return [False] * len(line)

    res = line
    number_indice = 0
    number = numbers[0]
    streak = 0

    for i in range(len(res)):
        if res[i] is None:
            if 0 < streak < number:  # en pleine série, on continue de la remplir
                res[i] = True
                streak += 1
            elif streak == number:  # fin d'une série, arrêt de celle-ci
                res[i] = False
                streak = 0
                if number_indice < len(numbers) - 1:  # Si il y as un prochain numéro, le prendre en compte
                    number_indice += 1
                    number = numbers[number_indice]
                else:  # Si il n'y as pas d'autres numéros, nous les avons alors tous mis, remplissage des cases restantes
                    return fill_list_with_falses(res)
            else:  # Si la case est vide et qu'aucun nombre est en cours, pas possible d'aller plus loin
                break
        elif res[i]:  # Si la case est vraie, incrément de la streak
            streak += 1
        else:
            if 0 < streak < number:  # Si une case bloquée interviens en milieu de streak, incohérence, ligne impossible
                raise Exception("line not possible")
            elif streak == number:
                streak = 0
                if number_indice < len(
                        numbers) - 1:  # Si c'est une fin de streak et qu'il y en as d'autres, passage au suivant
                    number_indice += 1
                    number = numbers[number_indice]
                else:  # Si on as fait tout les numéros, remplissage des vides
                    return fill_list_with_falses(res)
    return res


def fill_list_with_falses(l):
    for i in range(len(l)):
        if l[i] is None:
            l[i] = False
    return l


def fill_list_with_trues(l):
    for i in range(len(l)):
        if l[i] is None:
            l[i] = True
    return l


def process_fill_full_lines(line, numbers):
    if None not in line:
        return line
    if sum(numbers) + len(numbers) - 1 != len(line):
        return line

    line_indice = 0
    for number in numbers:
        for i in range(number):
            line[line_indice] = True
            line_indice += 1
        if line_indice < len(line) - 1:
            line[line_indice] = False
            line_indice += 1
    return line


def process_fill_completed_lines(line, numbers):
    working_line = fill_list_with_falses(line.copy())
    if sum(working_line) == sum(numbers):
        return working_line
    working_line = fill_list_with_trues(line.copy())
    if sum(working_line) == sum(numbers):
        return working_line
    return line


def process_fill_commons(line, numbers):
    if len(line) - (sum(numbers) + len(numbers)) >= max(numbers) or None not in line:
        return line

    current = []
    for i in range(len(line) - (sum(numbers) + len(numbers) - 1) + 1):
        work = [False] * i
        for number in numbers:
            work.extend([True] * number)
            work.append(False)
        if len(work) > len(line):
            work.pop()
        work.extend([False] * (len(line) - (sum(numbers) + len(numbers) - 1) - i - 1))

        if is_valid_line(line, work):
            if current == []:
                current = work.copy()
            else:
                for j in range(len(line)):
                    if current is not None and current[j] != work[j]:
                        current[j] = None
    if current:
        for i in range(len(line)):
            if line[i] is None:
                line[i] = current[i]
    return line
