from game_computer import compute_game, print_game

if __name__ == '__main__':
    print_game(compute_game(8, [[1, 1], [4], [4, 1], [7], [7], [4, 1], [4], [1, 1]], [[8], [6], [6], [8], [2], [2], [2], [1, 1]]))
