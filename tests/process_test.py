from processes import *


def process_fill_ends_tests():
    # Fully processed line test
    assert process_fill_ends([True] * 3 + [False] * 2, [1, 2, 5, 7, 4]) == [True] * 3 + [False] * 2

    # Empty line test
    assert process_fill_ends([None] * 6, []) == [False] * 6

    # Full line test
    assert process_fill_ends([True] + [None] * 6, [7]) == [True] * 7

    # Alternated line test
    assert process_fill_ends([False, True, None, None, None, False, True, None, None], [3, 2]) == \
           [False, True, True, True, False, False, True, True, False]


def process_fill_full_lines_tests():
    assert process_fill_full_lines([None] * 5, [5]) == [True] * 5
    assert process_fill_full_lines([None] * 5, [2, 2]) == [True] * 2 + [False] + [True] * 2
    assert process_fill_full_lines([None] * 15, [2, 3, 5, 2]) == [True] * 2 + [False] + [True] * 3 + [False] + [True] * 5 + [False] + [True] * 2


def fill_list_with_falses_test():
    assert fill_list_with_falses([None] * 5) == [False] * 5
    assert fill_list_with_falses([True] + [None] * 5) == [True] + [False] * 5


def process_fill_completed_lines_test():
    assert process_fill_completed_lines([None, True, None], [1]) == [False, True, False]


def process_fill_commons_test():
    assert process_fill_commons([None] * 15, [10]) == [None] * 5 + [True] * 5 + [None] * 5
    assert process_fill_commons([None] * 15, [7, 1]) == [None] * 6 + [True] * 1 + [None] * 8
    assert process_fill_commons([None] * 10 + [True] + [None] * 4, [1, 1]) == [None] * 10 + [True] + [None] * 4


if __name__ == '__main__':
    process_fill_ends_tests()
    process_fill_full_lines_tests()
    fill_list_with_falses_test()
    process_fill_completed_lines_test()
    process_fill_commons_test()
