def get_column(game, column_number):
    res = []
    for i in range(len(game)):
        res.append(game[i][column_number])
    return res


def set_column(game, column_number, column):
    for i in range(len(game)):
        game[i][column_number] = column[i]


def get_row(game, row_number):
    return game[row_number].copy()


def set_row(game, row_number, row):
    game[row_number] = row.copy()


def fill_numbers(array, length):
    for i in range(length):
        array.append([int(a) for a in input().split(";")])


def print_game(game):
    for row in game:
        for tile in row:
            if tile:
                print("█", end="")
            elif tile is None:
                print(" ", end="")
            else:
                print("X", end="")
        print()


def is_valid_line(current, new):
    for i in range(len(current)):
        if current[i] is not None and new[1] != current[i]:
            return False
    return True
